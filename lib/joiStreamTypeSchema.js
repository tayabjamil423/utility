const Joi = require("joi");

const streamTypeSchema = Joi.object({
  pipe: Joi.func().required(),
}).unknown();

module.exports = streamTypeSchema;
