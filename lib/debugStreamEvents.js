function debugStreamEvents(stream, logFunc, streamName) {
  let streamNameToUse = "";
  if (streamName) {
    streamNameToUse = `${streamName} - `;
  }

  stream.on("pipe", (pipeStream) => {
    logFunc(
      "%sEvent fired - name: pipe, Stream: %s",
      streamNameToUse,
      pipeStream.constructor.name
    );
  });

  stream.on("unpipe", (pipeStream) => {
    logFunc(
      "%sEvent fired - name: unpipe, Stream: %s",
      streamNameToUse,
      pipeStream.constructor.name
    );
  });

  stream.on("error", (error) => {
    logFunc.extend("error")(
      "%sEvent fired - name: error, error: %O",
      streamNameToUse,
      error
    );
  });

  const eventNames = [
    // These events trigger stream flow
    // "readable",
    // "data",

    "pause",
    "resume",

    "drain",

    "end",
    "finish",
    "close",
  ];

  for (const eventName of eventNames) {
    stream.on(eventName, () => {
      logFunc("%sEvent fired - name: %s", streamNameToUse, eventName);
    });
  }
}

module.exports = debugStreamEvents;
